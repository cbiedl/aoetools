aoetools for Debian
-------------------

With aoetools it is shipped an init script that will mount any AoE volume
properly configured in your /etc/fstab, that is, without option 'noauto'
AND with an option '_netdev', if you want to avoid any error messages in
the boot process.  In the following examples I am supposing that the rest
of the mount options are simply 'defaults', but you can replace it with
other parameters.

Example of a correct line:

/dev/etherd/e0.0p5  /mnt/aoe            xfs         defaults,_netdev            2   0


Wrong examples:

This line will generate boot errors, as the early mount process will try to mount this
volume, because it is not marked as network-dependant:

/dev/etherd/e0.0p5  /mnt/aoe            xfs         defaults                    2   0


This line will provoke that the /mnt/aoe volume won't simply get mounted while booting
or running "/etc/init.d/aoetools start":

/dev/etherd/e0.0p5  /mnt/aoe            xfs         defaults,noauto             2   0


Important note: Please note that if your network is not properly configured or your
AoE server is down, you can incur in big delays in the booting process.  If you fear so,
mount volumes manually after you are sure that your AoE shelf is reachable.
